package info.satscape.simukraft.common;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

//NOTE: Never used this
public class ItemGranulesCopper extends Item {
	private Icon icons[];
	
	public ItemGranulesCopper(int par1) {
		super(par1);
		maxStackSize = 64;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) {
		icons=new Icon[1];
		icons[0] = iconRegister.registerIcon("satscapesimukraft:granulesCopper");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return icons[0];
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return "Copper granules";
	}

	@Override
	public Icon getIcon(ItemStack stack, int pass) {
		return icons[0];
	}
	
}
