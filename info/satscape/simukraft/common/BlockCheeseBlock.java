package info.satscape.simukraft.common;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;

public class BlockCheeseBlock extends Block {
    @SideOnly(Side.CLIENT)
    private Icon[] icons;
    
	public BlockCheeseBlock(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setCreativeTab(CreativeTabs.tabBlock);
	}

	  @Override
	    @SideOnly(Side.CLIENT)
	    public void registerIcons(IconRegister iconRegister)
	    {
	        icons = new Icon[1];
	        icons[0] = iconRegister.registerIcon("satscapesimukraft:cheeseblock");
	    }

	    @Override
	    @SideOnly(Side.CLIENT)
	    public Icon getIcon(int side, int meta)      // getBlockTextureFromSideAndMetadata
	    {
	        return icons[0];
	    }
}
